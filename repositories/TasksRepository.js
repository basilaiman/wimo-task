const models = require("../models");

/**
 * @param sortBy column's name to sort (id by default)
 * @param sortDirection ASC or DESC
 * @returns all the tasks
 */
module.exports.findAll = function (sortBy, sortDirection){    
    return models.Tasks.findAll({
        order:[
            [sortBy,sortDirection]
        ]
    });
}
module.exports.findById = function (id){    
    return models.Tasks.findById(id);
}
module.exports.findByStatus = function (status,sortBy, sortDirection ){    
    return models.Tasks.findAll({
        where:{
            'status':status
        },
        order:[
            [sortBy,sortDirection]
        ]
    });
}
module.exports.updateStatusById = async function (id , status ){    
    let task  = await models.Tasks.findById(id);
    task.status = status;
    await task.save();
}
