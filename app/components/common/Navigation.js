import React, { Component } from 'react';
import { Dropdown } from 'react-bootstrap';
import { Link, Location } from 'react-router';
import Decoder from 'jwt-decode'
class Navigation extends Component {
    constructor(){
        super();
        this.state={
            "fullname":"Please log in"
        };
    }
    componentDidMount() {
        const { menu } = this.refs;
        $(menu).metisMenu();
    }

    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    secondLevelActive(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
    }

    render() {
        return (
            <nav className="navbar-default navbar-static-side" role="navigation">
                    <ul className="nav metismenu" id="side-menu" ref="menu">
                        <li className="nav-header">
                            <div className="dropdown profile-element"> 
                                <span></span>
                                <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                                    <span className="clear"> 
                                        <span className="block m-t-xs">                                             
                                        </span>
                                    </span> 
                                </a>                                
                            </div>
                        </li>
                        <li className={this.activeRoute("/Tasks")}>
                            <Link to="/Tasks"><i className="fa fa-th-large"></i> <span className="nav-label">Tasks</span></Link>
                        </li>
                      
                    </ul>

            </nav>
        )
    }
}

export default Navigation