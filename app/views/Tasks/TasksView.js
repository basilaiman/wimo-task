import React from 'react'
import MapModal from './MapModal';
import { fetchAllTasks } from '../../fetchers/TasksFetcher';
import Moment from 'moment';
import SearchAndSort from './SearchAndSort';
export default class extends React.Component{
    constructor(){
        super();
        //We're going to keep two copies of the results to filter locally and return it later.
        this.state={
            originalData:[],
            data:[],
            defaultMessage:"Loading",
            searchKeyword:"",
            showModal:false
        }
    }
    componentDidMount(){
        fetchAllTasks().then(({data})=>{
            this.setState({
                data,
                originalData:data});
        }).catch(()=> this.setState({defaultMessage : "There is no data available at the moment"}));
    }
    renderStatusField(status){        
        switch (status) {
            case "completed": return(<label className="label label-success">Completed</label>)
            case "failed": return(<label className="label label-danger">Failed</label>)
            case "started": return(<label className="label label-primary">Started</label>)
            case "pending": return(<label className="label label-warning">Pending</label>)                                
        }
    }
    renderData(){
        if(this.state.data.length == 0 )
            return (<tr><td>{this.state.defaultMessage}</td></tr>);
        else
            return this.state.data.map((e,i)=>(
                <tr key={i} 
                style={{cursor:"pointer"}}
                onClick={() => this.setState({showModal:true,selectedTask:e})}>
                    <td>{i+1}</td>
                    <td>{`${e['fromLocation'].coordinates[0]} , ${e['fromLocation'].coordinates[1]}`}</td>
                    <td>{`${e['toLocation'].coordinates[0]} , ${e['toLocation'].coordinates[1]}`}</td>
                    <td>{Moment(e.deliveryDate).format("YYYY-MM-DD")}</td>
                    <td>{(e.startedAt)?(Moment(e.startedAt).format("YYYY-MM-DD  HH:mm")):"-"}</td>
                    <td>{(e.finishedAt)?(Moment(e.finishedAt).format("YYYY-MM-DD  HH:mm")):"-"}</td>
                    <td>{e.driverName}</td>
                    <td>{e.courier}</td>
                    <td>{e.description}</td>
                    <td>{this.renderStatusField(e.status)}</td>
                    <td className="text-danger"><b>{e.driverComment}</b></td>
                    <td><a onClick={()=> this.props.router.push(`/tasks/${e.id}`)}><i className="fa fa-eye"></i></a></td>
                </tr>
            ));
    }
    //Preforms Serach with a keyword
    Search(keyword){
        keyword = keyword.toLowerCase();
        let data = this.state.originalData.slice(0); //Clone the data
        let filteredData = data.filter((e)=>{
            let text = e.driverName.toLowerCase() + " "
                       + e.courier.toLowerCase() + " "
                       + e.description.toLowerCase() + " "
                       + e.status.toLowerCase();
            return text.includes(keyword);
        });
        this.setState({data:filteredData});
    }
    onSearchChange(keyword){    
        this.setState({
            searchKeyword:keyword
        },()=>this.Search(keyword));        
    }
    onSortChange(sortBy,direction){
        this.setState({
            defaultMessage:"Loading"
        });

        fetchAllTasks(sortBy,direction).then(({data})=>{
            this.setState({
                data,
                originalData:data},()=> this.Search(this.state.searchKeyword));
        })
        .catch(()=> this.setState({defaultMessage : "There is no data available at the moment"}));
    }
    
    render(){
        return(
            <div>
                <MapModal   handleClose={()=>this.setState({showModal:false})} 
                            show={this.state.showModal} 
                            selectedTask={this.state.selectedTask} />
                <SearchAndSort onSearchChange={this.onSearchChange.bind(this)} onSortChange={this.onSortChange.bind(this)} />
                <div className="ibox">
                    <div className="ibox-title">
                        <h4>Tasks (click to see the line of the map)</h4>
                    </div>
                    <div className="ibox-content">
                        <table className="table table-responsive table-hover text-center">
                            <thead>
                                <tr>
                                    <td><b>#</b></td>
                                    <td><b>From</b></td>
                                    <td><b>To</b></td>
                                    <td><b>Delivery Date</b></td>
                                    <td><b>Started At</b></td>
                                    <td><b>Finished At</b></td>
                                    <td><b>Driver Name</b></td>
                                    <td><b>Courier</b></td>
                                    <td><b>Descripttion</b></td>
                                    <td><b>Status</b></td>
                                    <td><b>Driver Comment</b></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.renderData()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}