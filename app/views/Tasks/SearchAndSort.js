import React from 'react';
import PropTypes from 'proptypes';
import proptypes from 'proptypes';

/**
 *  This component takes two functions as a props
 *  onSearchChange(STRING keyword) this will be invoked with the keyword as an argument
 *  onSortChange(STRING sortBy,STRING direction) this will be invoked when whether the direction
 *  or the sort column changes
 */
class SearchAndSort extends React.Component{

    constructor(){
        super();
        this.state={
            sortBy:"",
            direction:"ASC"
        }
    }
    onSortChange(e){
        this.setState({
            sortBy:e.target.value
        },() => this.props.onSortChange(this.state.sortBy,this.state.direction))
    }
    onDirectionChange(e){
        this.setState({
            direction:e.target.value
        },() => this.props.onSortChange(this.state.sortBy,this.state.direction))
    }
    render(){        
        return(
            <div className="ibox">
                <div className="ibox-content">
                    <div className="col-xs-6">                    
                        <input onChange={(e) => this.props.onSearchChange(e.target.value)} type="text" className="form-control" placeholder="Search by Driver Name, Courier or Status"/>
                    </div>
                    <div className="col-xs-6">
                        <label htmlFor="">Sort by</label>
                        <select className="form-control" value={this.state.sortBy} onChange={this.onSortChange.bind(this)}>
                            <option value="deliveryDate"> Delivery Date</option>
                            <option value="Status"> Status</option>
                            <option value="Courier"> Courier</option>
                            <option value="startedAt"> Delivery start date</option>
                        </select>
                        <label htmlFor="">Direction</label>
                        <select className="form-control" value={this.state.direction} onChange={this.onDirectionChange.bind(this)}>
                            <option value="ASC">ASC</option>
                            <option value="DESC">DESC</option>                
                        </select>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        );
    }
}

SearchAndSort.propTypes = {
   onSearchChange : proptypes.func.isRequired,
   onSortChange:  proptypes.func.isRequired
}

export default SearchAndSort;