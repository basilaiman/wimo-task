import React from 'react'
import MapModal from './MapModal';
import { fetchById , updateStatusById } from '../../fetchers/TasksFetcher';
import Moment from 'moment';
import Map from './Map';
export default class extends React.Component{
    constructor(){
        super();
        this.state={
            task:undefined,
            selectedStatus:"started"
        }
        
    }
    componentDidMount(){
        let id = this.props.routeParams.id;
        console.log(this.props.router);
        fetchById(id).then(({data})=>{
            if(data == null) throw err; //To Avoid Empty data
            this.setState({task:data});
        }).catch(()=> this.props.router.push("/Tasks"));
    }
    renderStatusField(status){        
        switch (status) {
            case "completed": return(<label className="label label-success">Completed</label>)
            case "failed": return(<label className="label label-danger">Failed</label>)
            case "started": return(<label className="label label-primary">Started</label>)
            case "pending": return(<label className="label label-warning">Pending</label>)                                
        }
    }
   
    onUpdateClick(){
        let id = this.props.routeParams.id;
        let status = this.state.selectedStatus;
        updateStatusById(id,status)
        .then((response)=>{
            let task = this.state.task;
            task.status = status;
            this.setState({task});
            alert("The status has been updated successfully");
        })
        .catch((err)=>{
            //should be changed to a fancy error popup
            alert("An error has happened. please try again later or contact us");
        });
    }
    render(){
        const {task} = this.state;  
        let path = [];                    
        if(task){
            let fromCord = task.fromLocation.coordinates;
            let toCord = task.toLocation.coordinates;
            let from = { lat : fromCord[0] , lng : fromCord[1]};
            let to = { lat : toCord[0] , lng : toCord[1]};
            path = [from,to];
        }
        return(
            <div>
                
                <div className="ibox">
                    <div className="ibox-title">
                        <h4>Tasks</h4>
                    </div>
                    <div className="ibox-content">
                        <div className="row">
                            <div className="col-xs-12 col-md-3">
                                <div className="form-group">
                                    <label htmlFor="">From</label>
                                    <p htmlFor="">{task && task.fromLocation.coordinates.join(" , ")}</p>
                                </div>                     
                                <div className="form-group">
                                    <label htmlFor="">To</label>
                                    <p htmlFor="">{task && task.toLocation.coordinates.join(" , ")}</p>
                                </div>                  
                                <div className="form-group">
                                    <label htmlFor="">Delivery Date</label>
                                    <p htmlFor="">{task && Moment(task.deliveryDate).format("YYYY-MM-DD")}</p>
                                </div>                             
                                <div className="form-group">
                                    <label htmlFor="">Started At</label>
                                    <p htmlFor="">{task && task.startedAt && Moment(task.startedAt).format("YYYY-MM-DD  HH:mm")}</p>
                                </div>                             
                                <div className="form-group">
                                    <label htmlFor="">Finished At</label>
                                    <p htmlFor="">{task && task.finishedAt&& Moment(task.finishedAt).format("YYYY-MM-DD  HH:mm")}</p>
                                </div>                                   
                            </div>
                            <div className="col-xs-12 col-md-3">
                                <div className="form-group">
                                    <label htmlFor="">Driver Name</label>
                                    <p htmlFor="">{task && task.driverName}</p>
                                </div>                     
                                <div className="form-group">
                                    <label htmlFor="">Courier</label>
                                    <p htmlFor="">{task && task.courier}</p>
                                </div>                  
                                <div className="form-group">
                                    <label htmlFor="">Description</label>
                                    <p htmlFor="">{task && task.description}</p>
                                </div>                             
                                <div className="form-group">
                                    <label htmlFor="">Status</label>
                                    <p htmlFor="">{task && this.renderStatusField(task.status)}</p>
                                </div>                             
                                <div className="form-group">
                                    <label htmlFor="">Driver Comment</label>
                                    <p htmlFor="">{task && task.driverComment}</p>
                                </div>                                   
                            </div>   
                            <div className="col-xs-12 col-md-6">                            
                                {(task && <Map
                                    path={path}
                                    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                                    loadingElement={<div style={{ height: `100%` }} />}
                                    containerElement={<div style={{ height: `400px` }} />}
                                    mapElement={<div style={{ height: `100%` }} />}
                                    />)}
                            </div>                         
                        </div>                                                
                    </div>
                </div>
                {/* Updating Field  */}
                { task && task.status === "pending" && (
                    <div className="ibox">
                        <div className="ibox-title">
                            <h4>Update the status</h4>
                        </div>
                        <div className="ibox-content">
                            <p>Would you like to update the status of the task ?</p>
                            <div className="form-group">
                                <label htmlFor="">Choose Status</label>
                                <select onChange={(e) => this.setState({selectedStatus:e.target.value})} 
                                        className="form-control m-r-lg" 
                                        style={{width:"50%", display:"inline-block"}}>

                                    <option value="started">started</option>
                                    <option value="failed">failed</option>
                                    <option value="completed">completed</option>

                                </select>
                                <button className="btn btn-primary" onClick={this.onUpdateClick.bind(this)}>Update</button>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}