import React from "react"

import { withScriptjs, withGoogleMap, GoogleMap, Marker, Polyline } from "react-google-maps"
const MyMapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap defaultZoom={12} defaultCenter={props.path[0]} >

    {(props.path)&&(<Marker position={props.path[0]} />)}
    {(props.path)&&(<Marker position={props.path[1]} />)}
    {(props.path)&&(<Polyline path={props.path} geodesic={true} />)}
  </GoogleMap>
))

export default MyMapComponent;