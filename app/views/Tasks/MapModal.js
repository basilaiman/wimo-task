import React from 'react';
import {Modal, Button} from 'react-bootstrap';
import Map from './Map';
class MapModal extends React.Component {    

    render() {
        let path= [];
        if(this.props.selectedTask){
        let selectedTask = this.props.selectedTask;
        let fromCord = selectedTask.fromLocation.coordinates;
        let toCord = selectedTask.toLocation.coordinates;
        let from = { lat : fromCord[0] , lng : fromCord[1]};
        let to = { lat : toCord[0] , lng : toCord[1]};
        path = [from,to];
      }
      
      return (
        <div>          
          <Modal show={this.props.show} onHide={this.props.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <Map
                path={path}
                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
                />
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.handleClose}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
      );
    }
  }  
  export default MapModal;