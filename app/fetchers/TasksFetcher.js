import axios from 'axios';
export function fetchAllTasks(sortBy , direction){
    let path = "/api/tasks";
    if(sortBy || direction){ // If the values exist
        path += "?";
        path += (sortBy) ? `sortBy=${sortBy}&` : "";
        path += (direction) ? `direction=${direction}` : "";
    }
    return axios.get(path);
}
export function fetchById(id){
    let path = "/api/tasks/" + id;
    return axios.get(path);
}
export function fetchPendingTasks(status ,sortBy , direction){
    let path = `/api/tasks/status/${status}`;
    if(sortBy || direction){ // If the values exist
        path += "?";
        path += (sortBy) ? `sortBy=${sortBy}&` : "";
        path += (direction) ? `direction=${direction}` : "";
    }
    return axios.get(path);
}

export function updateStatusById(id,status){
    let path = `/api/tasks/${id}/status`;
    return axios.put(path,{status});
}