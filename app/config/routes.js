import React from 'react'
import Main from '../components/layouts/Main';

//----------------------
// App Views -----------
//----------------------
import TasksView from '../views/Tasks/TasksView';
import SingleTasksView from '../views/Tasks/SingleTaskView';





import { Route, Router, IndexRedirect, browserHistory} from 'react-router';

export default (
    <Router history={browserHistory}>
        <Route path="/" component={Main}>
            <IndexRedirect to="/Tasks" />
            <Route path="Tasks" component={TasksView}> </Route>   
            <Route path="Tasks/:id" component={SingleTasksView}> </Route>            
        </Route>
    </Router>

);