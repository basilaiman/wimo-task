const axios = require("axios").default;
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    //Seeding the data from the endpoint
    let response = await axios.get("https://api.myjson.com/bins/b9ix6");    
    //Converting points to GEOMETROY
    let tasks = response.data.tasks;
    //Converting the fields fromLocation and toLocation to a points
    tasks.forEach(task => {      
      //Converting points splited by ',' to MySql Point functions
      let fromCordinates = task.fromLocation.split(",");
      let toCordinates = task.toLocation.split(",");

      task.fromLocation = Sequelize.fn("Point",fromCordinates);
      task.toLocation = Sequelize.fn("Point",toCordinates);

      //Avoiding emptry dates
      task.startedAt = (task.startedAt === "")? null : task.startedAt;
      task.finishedAt = (task.finishedAt === "")? null : task.finishedAt;
    });
    return queryInterface.bulkInsert('Tasks',tasks , {});              
  },

  down: (queryInterface, Sequelize) => {    
      return queryInterface.bulkDelete('Tasks', null, {});    
  }
};
