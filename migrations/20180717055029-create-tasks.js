'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Tasks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fromLocation: {
        type: "Point"
      },
      toLocation: {
        type: "Point"
      },
      deliveryDate: {
        type: Sequelize.DATE
      },
      startedAt: {
        type: Sequelize.DATE,
        allowNull:true,
      },
      finishedAt: {
        type: Sequelize.DATE,
        allowNull:true,
      },
      driverName: {
        type: Sequelize.STRING
      },
      courier: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.STRING
      },
      driverComment: {
        type: Sequelize.STRING
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Tasks');
  }
};