var express = require('express');
var router = express.Router();
var TasksRepo = require("../repositories/TasksRepository");
/* GET users listing. */
router.get('/', function(req, res, next) {
    //Default soring value
    let sortBy = "id";
    let direction = "ASC";
    //Check if query string exists
    if(req.query){
      sortBy = req.query["sortBy"] || "id";
      direction = req.query["direction"] || "ASC";
    }
  
    TasksRepo.findAll(sortBy,direction)
      .then((r)=>res.json(r))
      .catch((err)=> res.status(400).jsonp({
          status:400,
          err : err.original.code,
          details: "Please make sure that all your paramters are correct"
      }));    
});
router.get('/:id', function(req, res, next) {
    let id = req.params.id;  

    TasksRepo.findById(id)
      .then((r)=>res.json(r))
      .catch((err)=> res.status(400).jsonp({
          status:400,
          err : err.original.code,
          details: "Please make sure that all your paramters are correct"
      }));    
});
router.get('/status/:status',function(req,res,next){
    //Default soring value
    let sortBy = "id";
    let direction = "ASC";
    //Check if query string exists
    if(req.query){
      sortBy = req.query["sortBy"] || "id";
      direction = req.query["direction"] || "ASC";
    }
    //Return the data filtered by the status
    let status = req.params.status;  
    TasksRepo.findByStatus(status,sortBy,direction)
    .then((r)=>res.json(r))
    .catch((err)=> res.status(400).jsonp({
        status:400,
        err : err.original.code,
        details: "Please make sure that all your paramters are correct"
    }));    

});
router.put("/:id/status",function(req,res,next){
    const {id} = req.params;
    const { status } = req.body;
    TasksRepo.updateStatusById(id,status);
    res.status(200).send();
});
module.exports = router;
