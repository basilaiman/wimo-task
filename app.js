var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//-------------------------------------------
// Importing  Routes ------------------------
//-------------------------------------------
var indexRouter = require('./routes/index'); 
var tasksRouter = require('./routes/tasks');

//-------------------------------------------
//Initaliziting the application  ------------
//-------------------------------------------
var app = express();

//-------------------------------------------
// Applying Middlewares ---------------------
//-------------------------------------------
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//-------------------------------------------
// Configuring Routes -----------------------
//-------------------------------------------
app.use('/', indexRouter); //Entry point to the web app
app.use('/api/tasks', tasksRouter); // ../api/tasks/

module.exports = app;
