'use strict';
module.exports = (sequelize, DataTypes) => {
  var Tasks = sequelize.define('Tasks', {
    fromLocation: { type: 'Point'},
    toLocation: { type: 'Point'},
    deliveryDate: DataTypes.DATE,
    startedAt: DataTypes.DATE,
    finishedAt: DataTypes.DATE,
    driverName: DataTypes.STRING,
    courier: DataTypes.STRING,
    description: DataTypes.TEXT,
    status: DataTypes.STRING,
    driverComment: DataTypes.STRING
  }, {
    timestamps:false
  });
  Tasks.associate = function(models) {
    // associations can be defined here
  };
  return Tasks;
};