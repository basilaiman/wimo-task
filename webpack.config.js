var path = require("path");
var webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry:'./app/index.js',
    output:{
        path:path.resolve(__dirname,'public'),
        filename:'bundle.js',
        publicPath:"/"
    },
    devServer:{
        contentBase:path.join(__dirname,"public"),
        compress:true,
        port:9000
    },
    module:{
        rules:[
            { 
                 test: /\.js$/,
                 exclude: /node_modules/,
                 loader: "babel-loader" 
            },
            { 
                test: /\.css$/,
                use: ExtractTextPlugin.extract({fallback: "style-loader",use: "css-loader", publicPath:'/'})
            },
            {
                test: /\.(png|jpg|gif)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.(eot|com|json|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
            }
        ]
    },
    plugins: [new HtmlWebpackPlugin({
      title: 'Wimo Task',
      template: './app/index.html',      
      filename:'../public/index.html'
    }),
    new ExtractTextPlugin({
        filename:'style.css',
        disable:false,
        allChunks:true
        }),
    new webpack.ProvidePlugin({
            '$': "jquery",
            'jQuery': "jquery",
            'window.jQuery': "jquery",
            'window.$': 'jquery'

        })]

    
};